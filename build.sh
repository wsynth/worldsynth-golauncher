echo "Building linux launcher"
eval "GOOS=linux GOARCH=amd64 go build launcher.go"
mv "launcher" "launcher-linux"
echo "Done building linux launcher"

echo "Building windows launcher"
eval "GOOS=windows GOARCH=amd64 go build launcher.go"
mv "launcher.exe" "launcher-windows.exe"
echo "Done building windows launcher"

echo "Building mac launcher"
eval "GOOS=darwin GOARCH=amd64 go build launcher.go"
mv "launcher" "launcher-mac"
echo "Done building mac launcher"