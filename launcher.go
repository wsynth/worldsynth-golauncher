// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
)

func main() {
	log.Printf("Execution directory: %s", GetFilepath("."))
	for index, argument := range os.Args {
		log.Printf("Argument[%d]: %s\n", index, argument)
	}

	log.Println("Init/Read config")
	config := GetConfig()

	jrePath, jreerr := GetJre(config, runtime.GOOS)
	if jreerr != nil {
		log.Println("No JRE was provided, trying to run on system JRE if avaiable")
		jrePath = "java"
	} else {
		log.Printf("trying to run on JRE found at %s\n", jrePath)
	}

	// Create command args starting with JVM memmory args
	cmdArgs := []string{config.JRE_XMS, config.JRE_XMX}

	// Add other JVM args
	cmdArgs = append(cmdArgs, config.DEFAULT_JVM_OPTS...)

	// Add classpath arg
	classpath := "Test.jar:" + GetFilepath("lib") + "/*"
	if runtime.GOOS == "windows" {
		classpath = "Test.jar;" + GetFilepath("lib") + "/*"
	}
	cmdArgs = append(cmdArgs, "-cp")
	cmdArgs = append(cmdArgs, classpath)

	// Add main class arg
	cmdArgs = append(cmdArgs, config.MAIN)

	// create command
	cmd := exec.Command(jrePath, cmdArgs...)
	log.Print(cmd)

	// Monitoring pipes for command results
	var stderr bytes.Buffer
	cmd.Stderr = &stderr
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)
	}

	// Execute command
	log.Printf("Trying to start WorldSynth")
	err = cmd.Start()
	if err != nil {
		log.Println(fmt.Sprint(err) + ": " + stderr.String())
		log.Printf("Press enter to finish ...")
		reader := bufio.NewReader(os.Stdin)
		reader.ReadString('\n')
		log.Fatal("Exit")
	}

	// Print out logs to teminal
	scanner := bufio.NewScanner(stdout)
	go func() {
		for scanner.Scan() {
			fmt.Println(scanner.Text())
		}
	}()

	// Keep terminal logging open?
	if config.PERSIST_TERM {
		err = cmd.Wait()
		log.Printf("Finished with error: %v", err)
	}
}

type Config struct {
	JRE_WINDOWS      string
	JRE_LINUX        string
	JRE_MAC          string
	JRE_XMS          string
	JRE_XMX          string
	DEFAULT_JVM_OPTS []string
	MAIN             string
	PERSIST_TERM     bool
}

const configFile string = "./launchconfig.json"

func GetConfig() Config {
	var config = createDefaultConfig()
	if configExists() {
		config = readConfig()
		log.Println("Config read from file")
	} else {
		log.Println("Config initialized as default")
	}
	log.Printf("Config:\n%s\n", config)

	return config
}

func GetJre(config Config, os string) (string, error) {
	log.Printf("System OS is %s\n", os)
	switch os {
	case "windows":
		log.Printf("Config JRE path: %s\n", config.JRE_WINDOWS)
		if config.JRE_WINDOWS == "system-jre" || config.JRE_WINDOWS == "" {
			return config.JRE_WINDOWS, errors.New("There is no JRE path provided")
		}
		return GetFilepath(config.JRE_WINDOWS), nil
	case "linux":
		log.Printf("Config JRE path: %s\n", config.JRE_LINUX)
		if config.JRE_LINUX == "system-jre" || config.JRE_LINUX == "" {
			return config.JRE_LINUX, errors.New("There is no JRE path provided")
		}
		return GetFilepath(config.JRE_LINUX), nil
	case "darwin":
		log.Printf("Config JRE path: %s\n", config.JRE_MAC)
		if config.JRE_MAC == "system-jre" || config.JRE_MAC == "" {
			return config.JRE_MAC, errors.New("There is no JRE path provided")
		}
		return GetFilepath(config.JRE_MAC), nil
	default:
		log.Println("There was not found any JRE config for this OS")
		return "", errors.New("There is no JRE path provided")
	}
}

func createDefaultConfig() Config {
	return Config{
		"system-jre",
		"system-jre",
		"system-jre",
		"-Xms512M",
		"-Xmx4G",
		[]string{"--add-exports=javafx.base/com.sun.javafx.event=ALL-UNNAMED",
			"--add-exports=javafx.graphics/com.sun.javafx.stage=ALL-UNNAMED",
			"--add-exports=javafx.controls/com.sun.javafx.scene.control.behavior=ALL-UNNAMED",
			"--add-opens=java.base/java.lang.reflect=ALL-UNNAMED"},
		"net.worldsynth.patcher.WorldSynthPatcher",
		false}
}

func configExists() bool {
	return FileExists(configFile)
}

func readConfig() Config {
	bytes, _ := FileRead(configFile)
	var config Config
	json.Unmarshal(bytes, &config)
	return config
}

func writeConfig(config Config) {
	bytes, _ := json.Marshal(config)
	FileWrite(configFile, bytes, false)
}

//FileRead reads a file with the filename into a propperly sized bytearray.
//Will return error if a file with the given file does not exist.
func FileRead(filename string) ([]byte, error) {
	if !FileExists(filename) {
		return nil, errors.New("Tried to read file with name \"" + filename + "\" but there was no such file.")
	}

	return ioutil.ReadFile(GetFilepath(filename))
}

//FileWrite writes data to a given file.
func FileWrite(filename string, data []byte, overwrite bool) error {
	if FileExists(filename) && !overwrite {
		return errors.New("Tried  write data to new file with name \"" + filename + "\" but the file already exists")
	}

	return ioutil.WriteFile(GetFilepath(filename), data, 0644)
}

//FileExists is used to check if a file with the given filename exists
func FileExists(filename string) bool {
	_, err := os.Stat(GetFilepath(filename))
	if err != nil {
		return false
	}
	return true
}

func GetFilepath(filename string) string {
	execFilePath, _ := os.Executable()
	execDirPath := filepath.Dir(execFilePath)
	filePath := filepath.Join(execDirPath, filename)
	return filePath
}
